#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 13:51:18 2017

@author: Tristan de BLAUWE
"""

"""
===== IMPORT =====
"""
import numpy as np
from collections import namedtuple

"""
===== FONCTIONS =====
"""


def randomTask(DurationMin, DurationMax, Machines):
    """
    ENTREE:
            * DurationMin - Integer - Durée minimale (compris)
            * DurationMax - Integer - Durée maximale (compris)
            * Machines - Tableau des mâchines
    SORTIE: Une tâche aléatoire selon les paramètres données
    """
    DurationPerMachine = np.zeros((Machines.size),dtype=int)
    for indice,Machine in enumerate(Machines):
        DurationPerMachine[indice] = np.random.randint(DurationMin,DurationMax+1,1)
    task = Task(DurationPerMachine)
    
    return task

def randomTasksFactory(Size, DurationMin, DurationMax, Machines):
    """
    ENTREE: 
            * Size - Taille de la liste de tâches à créer
            * DurationMin - Integer - Durée minimale (compris)
            * DurationMax - Integer - Durée maximale (compris)
            * Machines - Tableau des mâchines
    SORTIE: Une liste de tâches aléatoire
    """
    tasks = []
    for indice in range(Size) :
        tasks.append(randomTask(DurationMin,DurationMax,Machines))
    return tasks

def generateTasksFromArray(tasksIn):
    tasks = []
    for task in tasksIn:
        tasks.append(Task(task))
    return tasks

def printTasks(Tasks):
    for task in Tasks:
        print(task)
    return 

def minTasks(Tasks):
    """
    ENTREE: Tableau de tâches
    SORTIE: Tâches dont la durée sur une machine est la plus courte, avec l'indice pour faciliter la suppresion de cette tâche
    """
    IndiceMinTask = -1
    DurationMin = 0
    CurrentDuration = 0
    Machine = 0
    for indice, task in enumerate(Tasks):
        if IndiceMinTask == -1:
            IndiceMinTask = indice
            DurationMin = task.DurationPerMachine.min()
            Machine = task.DurationPerMachine.argmin()
        CurrentDuration = task.DurationPerMachine.min()
        if CurrentDuration < DurationMin:
            IndiceMinTask = indice
            DurationMin = CurrentDuration
            Machine = task.DurationPerMachine.argmin()
    return (Tasks[IndiceMinTask],Machine,IndiceMinTask)

def Johnson(Tasks):
    """
    ENTREE: Un tableau de tâches
    SORTIE: Une liste d'ordonnancement des tâches données, selon l'algoritme de Johnson
    """
    G = []
    D = []

    while len(Tasks) > 0:
        (task, MachineMin,Indice) = minTasks(Tasks)
        if MachineMin == 0:
            G.append(task)
        else:
            D.insert(0,task)
        del Tasks[Indice]

    return (G + D)

def calculTpsExec(Ordonnacement,nbMachine):
    """
    ENTREE: Un ordonnacement de tâches
    SORTIE: (tpsExecPerMachine, DateFin)
            * tpsExecPerMachine - Temps d'éxécution pour chaque machine
            * DateFin - Date de fin d'éxécution pour cette ordonnancement
    """
    TimePerMachine = np.zeros(nbMachine,dtype=int)

    for task in Ordonnacement:
        for indice in range(len(TimePerMachine)):
            if indice == 0:
                TimePerMachine[indice] += task.DurationPerMachine[indice]
            elif indice > 0:
                TimePerMachine[indice] = task.DurationPerMachine[indice] + max(TimePerMachine[indice],TimePerMachine[indice-1])
#                if TimePerMachine[indice-1] > TimePerMachine[indice]:
#                    TimePerMachine[indice] = task.DurationPerMachine[indice] + (TimePerMachine[indice-1]-TimePerMachine[indice])
#                else:
#                    TimePerMachine[indice] += task.DurationPerMachine[indice]
    DateFin = TimePerMachine.max()
    return (TimePerMachine,DateFin)

"""
===== MAIN =====
"""

# Définition d'un type tâche
Task = namedtuple("Task", ["DurationPerMachine"])

# Paramètres
nbMachines      = 3
TasksSize       = 3
DurationMin     = 1
DurationsMax    = 5

Machines        = np.arange(nbMachines)

# tasks - Tableau des tâches
tasks = randomTasksFactory(TasksSize,DurationMin,DurationsMax,Machines)
# ~~~~~ OR ~~~~~
# tasksIn = readFromFile()
# tasks = generateTasksFromArray(tasksIn)

Ordo = Johnson(tasks)
(TimePerMachine,DateFin) = calculTpsExec(Ordo, nbMachines)

# ==== AFFICHAGE RESULTAT ====
print("=== Ordonnacement étudié : ")
print(Ordo)
print("\n")
print("=== Temps d'éxécution pour chaque machine : ")
print(TimePerMachine)
print("\n")
print("=== Date de fin pour cette ordonnancement : ")
print(DateFin)
print("\n")
# ==== FIN ====

    
